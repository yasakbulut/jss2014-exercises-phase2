package tr.com.obss.jss2014.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/InfoServlet")
public class InfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String contextPath = request.getContextPath();
		String servletPath = request.getServletPath();
		Map<String, String[]> parameterMap = request.getParameterMap();
		Date date = new Date();
		StringBuilder builder = new StringBuilder();
		builder.append("Context Path: ").append(contextPath).append("\n");
		builder.append("Servlet Path: ").append(servletPath).append("\n");
		builder.append("Parameters: ").append(getStringFromParameters(parameterMap)).append("\n");
		builder.append("Date: ").append(date).append("\n");
		response.getOutputStream().write(builder.toString().getBytes());
		
	}
	
	private String getStringFromParameters(Map<String, String[]> parameterMap){
		StringBuilder builder = new StringBuilder();
		builder .append("{").append("\n\t");
		for (String parameter : parameterMap.keySet()) {
			builder.append(parameter).append(": ")
			.append(Arrays.toString(parameterMap.get(parameter))).append("\n\t");
		}
		builder.append("}");
		return builder.toString();
	}

}
